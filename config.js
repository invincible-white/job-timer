
let isProcEnv = (process.env.RUN_MODULE === 'proc')    //运行环境：dev|proc
console.log('job-timer module : ' + (isProcEnv ? 'proc' : 'dev'))

module.exports = {
   isProcEnv,
   listenPort : 4000,                       //监听端口

   //文件上传临时目录，通常指向 public 目录的绝对路径。
   upload: isProcEnv ? {
      publishDir: '/var/project/job-timer/public',
      urlPrefix: ''
   } : {
      publishDir: '/home/lzt/workspace/nodejs/job-timer/public',
      urlPrefix: ''
   },

   // datax home 目录
   dataxHome : isProcEnv ? '/opt/datax' : '/home/lzt/centos-home/root/workspace/bigdata/datax',

   // apache livy api 地址
   livy : isProcEnv ? {    //生产环境
      url: 'http://10.57.98.136:8998'
   } : {    //开发环境
      url: 'http://localhost:8998'
   },

   //mongodb 配置
   mongo: {
      url: isProcEnv ? 'mongodb://localhost:27017/job' :  'mongodb://localhost:27017/job',
      options: {
         connectTimeoutMS: 5000,
         socketTimeoutMS: 5000,
      }
   },

   //日志配置
   log4js : isProcEnv ? {    //生产环境
      appenders : {
         app: {
            type: 'file',
            filename: '/var/log/job-timer/app.log',
            maxLogSize: 10240000,
            backups: 30,
         }
      },
      categories: {
         default: {
            appenders: ['app'],
            level: 'DEBUG'
         }
      },
      pm2: true
   } : {    //开发环境
      appenders : {
         app: {
            type: 'file',
            filename: 'logs/app.log',
            maxLogSize: 1024000000,
            backups: 30,
         },
         console: { type: 'console' }
      },
      categories: {
         default: {
            appenders: ['app', 'console'],
            level: 'DEBUG'
         }
      }
   }
};